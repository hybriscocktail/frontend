# Consideraciones #

## Imágenes ##

Las imágenes asociadas a elementos configurables se envían en archivos PNG independientes. Tal es el caso de: iconos de categorías, logo, badges de características de los artículos (en oferta, con receta, natural, etc), iconos de redes sociales e imagen de avatar genérico. El resto de elementos gráficos propios del diseño de la interfaz se envían dentro de un archivo PNG único (sprite.png) para reducir el número de conexiones y mejorar los tiempos de carga. Tal es el caso de: carrito, lupa, cerrar, iconos del área privada de usuario, flechas y otros símbolos.

Los iconos de sección están en dos versiones: en color para el header y en azul para el footer con el sufijo '_b'.

#### Retina ####

Ademas, todas las imágenes van en versión básica y versión para pantallas retina con el sufijo '@@2x'. En el caso de las imágenes por CSS como el caso del sprite, se controlan automáticamente con MediaQueries; mientras que en el caso de las etiquetas <IMG> se controlan automáticamente con javascript. Todo el control es transparente, el único requisito es que las imágenes para retina existan en la misma ubicación que sus originales y contengan el sufijo '@@2x' entre el nombre y el punto y su extensión.

## Las categorías ##

Para cada categoría, existe un color asociado.

Se debe mencionar, que el bloque de título de página con su miga de pan superior, tiene una línea gruesa en su margen izquierdo, que es azul de base, pero adquiere el color de cada categoría cuando nos encontramos en una de ellas. Para establecer este color, se debe utilizar una clase CSS en el <DIV> que contiene el bloque. Por ejemplo:

```
#!html
<div class="page-header belleza">
```

El listado de clases es:

* medicamentos
* cuidado
* natural
* vitaminas
* dermo
* belleza
* bebes
* alimentos
* sexual

# Builds #

Cada entregable constará principalmente de:

* Una hoja de estilo.
* Un JS con toda la funcionalidad del front. No incluye la funcionalidad operativa.
* Carpeta de imágenes.
* Carpeta de fuentes tipográficas.

La hoja de estilo y el javascript deberán ser reemplazados en cada entrega. Además las imágenes y las fuentes tipográficas deberán ser actualizadas. En especial la imagen 'sprite.png'.

Además cada entregarle incluirá las maquetas html que servirán de referencia para la integración del front en la plataforma.

## v.1.5.0 ##

* Añadido modulo newsletter 
* Añadido vista de notificaciones

## v.1.4.0 ##

* Añadida galería a las fichas de producto para tener más de una imagen además de la opción de zoom.

## v.1.2.2 ##

* Cambios en la interacción del programa de lealtad.
* Añadidos los niveles de lealtad.
* Añadido mensaje para actualizar carrito.
* Actualizadas algunas páginas.

## v.1.2.1 ##

* Cambios en la interacción del programa de lealtad.

## v.1.2.0 ##

Cambios de la revisión:

* Modificado el header.
* Modificada la home.
* Modal de registro: paso1, paso2 y mensaje de bienvenida.
* Modificadas las páginas de Programa en Equilibrio en el área privada.
* Modificada la página de pago en el checkout.
* Modificado el detalle de producto para añadir dos bloques de productos relacionados.
* Añadida a la home la pestaña de promociones.

## v.1.1.0 ##

Correcciones y cambios de la revisión:

* Maquetado el botón de Facebook para compartir desde la ficha de productos.
* Corrección de estilos en los acordeones.
* Datos de dirección adicionales collapsables en todos los dispositivos.
* Corrección de color en link de ventana modal; estaba en amarillo.
* Corrección del mostrado y ocultado de los filtros en mobile.
* Corrección de los tags de emails y teléfonos cuando se muestran en ventanas modales; se desalineaba el botón de eliminar.
* Propuesta de corrección del menú para que no se mueva a dos filas.
* Propuesta de corrección para que los campos <select> en ventanas modales y cuando se ven desde mobile, no pierdan el foco y el scroll pase a la página de fondo.
* Aumentado el tamaño de los textos más pequeños.
* Reestructurado y refactorizado de todo el javascript.
* Eliminación de las categorías en el footer.
* Eliminada la palabra "Registro en" del link al "Programa en Equilibrio" del header.

## v.1.0.0 ##

Se incluyen las pantallas de Programa en Equilibrio.

## v.0.9.0 ##

Se incluye la home y el historial de pedidos del área privada.

#### Header ####

* Se ha movido el texto de envío gratis para ponerlo en la franja superior verde.
* Se ha reducido el tamaño del logo tanto a nivel archivo de imagen, como propiedades html y css.
* Se ha apretado el header para reducir el alto.

#### Área Privada ####

* Cambios en Mi perfil.
* Cambios en Direcciones de envío.
* Cambios en Datos de facturación.

#### Checkout ####

* Información de envío: el formulario de nueva dirección de envío, recibe los mismos cambios que en el área privada.
* Forma de pago: los formularios para editar un RFC o crear una nuevo, recibe los mismos cambios que en el área privada.
* Página de ok: se ajustó el tamaño de dos textos.

#### Formulario de registro ####

* Se ha modificado ligeramente el marcado para evitar que los textos de ayuda bajo los input, rompieran la rejilla de filas y columnas.

#### Correcciones generales ####

* Se ha modificado la manera en que se fijan las cajas resumen del sidebar del carrito y el proceso de checkout, para solucionar los problemas en algunos navegadores.
* Se han cambiado las propiedades css de zoom por transform.

## v0.8.2 ##

Se cambia el flujograma de la página de checkout. Ahora la disponibilidad de los artículos se muestra de inicio y si todos están disponibles se muestran las opciones de: envío express o envío programado; mientras que si algún artículo no está disponible, se muestran las opciones: envío express, envío parcial programado y envío completo programado.

## v0.8.1 ##

Correcciones en la ficha de producto y la ventana de checkout ok.

## v0.8 ##

Se incluye la ficha de producto.

#### Footer ####

* Se cambia el logo de Accertify.

## v0.7.1 Segunda parte del checkout y facturación ##

Correcciones menores.

#### General #####

* Se han eliminado los divs de las ventanas modales de todas las plantillas salvo el index.html que es el único lugar desde dónde se invocan.
* Se ha cambiado el icono de Accertify del footer y se ha eliminado el de Synthesis.

#### Área privada #####

* Se han homogeneizado los formularios que incluyen datos postales, dejando calle, número exterior e interior en una única línea. Además de unificado los textos de restricción de zona de reparto.
* Se ha cambiado el icono de facturación. Antes era un perfil humano sobre un cuadrado y ahora es una factura.

## v0.7.0 Segunda parte del checkout y facturación ##

Plantilla de la segunda parte del checkout: pago. Además de la plantilla de la confirmación de pago y la plantilla de factura online.

## v0.6.0 Cambios en área privada de usuario y registro ##

Cambios en las ventanas de registro y login. Ajustes en el flujo del área privada de usuario. Y mejoras en el checkout.

## v0.5.0 Carrito de compra ##

Primera parte del checkout.

#### Área privada #####

* Se han eliminado las leyendas de (opcional) y añadido una única de '*campos requeridos' de los formularios de edición y creación de dirección y métodos de pago.
* En el formulario de creación y edición de métodos de pago, se agregaron los campos de tarjeta de crédito. Lo mismo se hizo en las cajas del índice.
* En los índices de direcciones de envío y métodos de pago, en el link de eliminar, se eliminó la clase 'text-muted' y se añadió un marcado <small>.

#### Checkout ####

* Se añadió el atributo autofocus al <input> del código de descuento.

## v0.4.0 Carrito de compra ##

Carrito de compra: página de carrito, minicarrito del header y popup de artículo añadido al carrito.

#### Header #####

* Se han hecho algunos cambios para la integración del minicarrito.

#### Listado de artículos ####

* Se ha añadido una class en la etiqueta de ordenar.

## v0.3.0 Formularios modales de registro y primeras páginas del área privada ##

Los formularios modales, de momento se enlazan directamente desde la home. Donde además se han dejado únicamente los accesos a la página de artículos y el área privada, que son las únicas maquetadas hasta el momento. Respecto al área privada, sólo son definitivas las tres primeras secciones: resumen, direcciones y métodos de pago. Aunque se incluye también una versión temprana de la sección del histórico de pedidos, que aún no es definitiva.

En las secciones de direcciones y métodos de pago, el link de edición redirige a una nueva página. Aunque el flujo de uso recoge que el formulario de edición se abre directamente en la misma página, dentro de su caja. Siendo sólo el formulario de creación el que se abre en una nueva página.

#### Header ####

* Modificado el icono de Alimentos y bebidas.

## v0.2.0 Header y footer ##

Header y footer definitivos.

#### Listado de artículos ####
* Se modifica el badge de 'En oferta' para utilizar <IMG> en vez de ponerlo por CSS desde el sprite.
* Se elimina el precio anterior.
* Se cambia la leyenda del IVA.

## v0.1.1 Listado de artículos. ##

#### Listado de artículos ####
* Se añade una clase a las cajas de los filtros del sidebar para poner scroll a partir de cierta altura.
* Se añade el filtro de precio en el sidebar.
* Se cambia la leyenda del IVA.
* Se cambian los textos de los primeros artículos para ilustrar diferentes casos y se añaden nuevos artículos. (No afecta a integración)

## v0.1.0 Listado de artículos. ##

Maquetación del listado de artículos. Se incluye también una aproximación del header para dar un poco de contexto a la maqueta.